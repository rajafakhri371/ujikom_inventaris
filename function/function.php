<?php 

class database{

	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "uji_kom";
	var $koneksi;
	function __construct(){
		$this->koneksi = mysqli_connect($this->host, $this->uname, $this->pass,$this->db);
	}

	// ------------- Login -----------------------

	// function login_masuk($username,$password){

	// 	$sql = mysqli_query($this->koneksi,"SELECT * FROM petugas INNER JOIN level ON petugas.id_level=level.id_level WHERE username='$username' AND password='$password'");
		
	// 	$temukan =mysqli_num_rows($sql);
	// 	$data = mysqli_fetch_array($sql);

	// 	if ($temukan==1) {
	// 	session_start();

	// 	if ($data['nama_level']=="Administrator") {
	// 		$_SESSION['Administrator'] =$data['id_petugas'];
	// 		$_SESSION['id_petugas'] = $data['id_petugas'];
	// 		$_SESSION['nama_petugas'] =$data['nama_petugas'];
	// 		$_SESSION['username'] = $data['username'];
	// 		$_SESSION['password'] = $data['password'];
	// 	}elseif ($data['nama_level']=="Petugas") {
	// 		$_SESSION['Petugas'] = $data['id_petugas'];
	// 		$_SESSION['id_petugas'] = $data['id_petugas'];
	// 		$_SESSION['nama_petugas'] =$data['nama_petugas'];
	// 		$_SESSION['username'] = $data['username'];
	// 		$_SESSION['password'] = $data['password'];	
	// 	}else{
	// 		echo"<script type='text/javascript'>
 //                	alert('Login Gagal');
 //           	 		</script>";
	// 	}	
			
	// 	}
	// }

	// ------------- Akhir Login -----------------

	// ------------- CRUD Ruangan -----------------
	function tampil_data_ruang(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM ruang");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
    }

    function tambah_ruang($nama_ruang,$kode_ruang,$keterangan){
		mysqli_query($this->koneksi,"INSERT INTO ruang values('','$nama_ruang','$kode_ruang','$keterangan')");
    }	
    
    function hapus_ruang($id_ruang){
        mysqli_query($this->koneksi,"DELETE FROM ruang WHERE id_ruang='$id_ruang'");
    }
    
    function edit_ruang($id_ruang){
		$data = mysqli_query($this->koneksi,"SELECT * FROM ruang WHERE id_ruang='$id_ruang'");
		while($d = mysqli_fetch_array($data)){
			$hasil[] = $d;
		}
		return $hasil;
	}
 
	function update_ruang($id_ruang,$nama_ruang,$kode_ruang,$keterangan){
		mysqli_query($this->koneksi,"UPDATE ruang set nama_ruang='$nama_ruang', kode_ruang='$kode_ruang', keterangan_r='$keterangan' where id_ruang='$id_ruang'");
	}
	// ------------- Akhir CRUD Ruangan -----------------

	// -------------  CRUD Jenis -----------------

	function tampil_data_jenis(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM jenis");
		while($j = mysqli_fetch_array($data)){
			$hasil[] = $j;
		}
		return $hasil;
    }

    function tambah_jenis($nama_jenis,$kode_jenis,$keterangan){
		mysqli_query($this->koneksi,"INSERT INTO jenis values('','$nama_jenis','$kode_jenis','$keterangan')");
    }	

    function hapus_jenis($id_jenis){
        mysqli_query($this->koneksi,"DELETE FROM jenis WHERE id_jenis='$id_jenis'");
    }

    function edit_jenis($id_jenis){
		$data = mysqli_query($this->koneksi,"SELECT * FROM jenis WHERE id_jenis='$id_jenis'");
		while($j = mysqli_fetch_array($data)){
			$hasil[] = $j;
		}
		return $hasil;
	}
 
	function update_jenis($id_jenis,$nama_jenis,$kode_jenis,$keterangan){
		mysqli_query($this->koneksi,"UPDATE jenis set nama_jenis='$nama_jenis', kode_jenis='$kode_jenis', keterangan_j='$keterangan' where id_jenis='$id_jenis'");
	}

	// ------------- Akhir CRUD Jenis -----------------

	// -------------  CRUD Petugas -----------------

	function tampil_data_petugas(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM petugas INNER JOIN level ON petugas.id_level=level.id_level");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
    }

    function tampil_data_level(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM level");
		while($lv = mysqli_fetch_array($data)){
			$hasil[] = $lv;
		}
		return $hasil;
    }

    function tambah_petugas($nama_petugas,$username,$password,$level){
		mysqli_query($this->koneksi,"INSERT INTO petugas (nama_petugas,username,password,id_level) values('$nama_petugas','$username','$password','$level')");
    }

     function hapus_petugas($id_petugas){
        mysqli_query($this->koneksi,"DELETE FROM petugas WHERE id_petugas='$id_petugas'");
    }

    function edit_petugas($id_petugas){
		$data = mysqli_query($this->koneksi,"SELECT * FROM petugas WHERE id_petugas='$id_petugas'");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}
 
	function update_petugas($id_petugas,$nama_petugas,$username,$password,$level){
		mysqli_query($this->koneksi,"UPDATE petugas set nama_petugas='$nama_petugas', username='$username', password='$password',id_level='$level' where id_petugas='$id_petugas'");
	}

	// ------------- Akhir CRUD Petugas -----------------

	// -------------  CRUD Pegawai -----------------

	function tampil_data_pegawai(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM pegawai");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
    }

    function tambah_pegawai($nama_pegawai,$username,$password,$nip,$alamat){
		mysqli_query($this->koneksi,"INSERT INTO pegawai (nama_pegawai,username,password,nip,alamat) values('$nama_pegawai','$username','$password','$nip','$alamat')");
    }

     function hapus_pegawai($id_pegawai){
        mysqli_query($this->koneksi,"DELETE FROM pegawai WHERE id_pegawai='$id_pegawai'");
    }

    function edit_pegawai($id_pegawai){
		$data = mysqli_query($this->koneksi,"SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}
 
	function update_pegawai($id_pegawai,$nama_pegawai,$username,$password,$nip,$alamat){
		mysqli_query($this->koneksi,"UPDATE pegawai set nama_pegawai='$nama_pegawai', username='$username', password='$password',nip='$nip',alamat='$alamat' where id_pegawai='$id_pegawai'");
	}

	// ------------- Akhir CRUD Pegawai -----------------

	// ------------- Inventaris -------------------------

    function sarana(){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM sarana");
    	while($j = mysqli_fetch_array($data)){
			$hasil[] = $j;
		}
		return $hasil;
    }

    function in_jenis(){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM jenis");
    	while($js = mysqli_fetch_array($data)){
			$hasil[] = $js;
		}
		return $hasil;
    }

    function in_ruang(){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM ruang");
    	while($ru = mysqli_fetch_array($data)){
			$hasil[] = $ru;
		}
		return $hasil;
    }
    // INVENTARIS
    function detail($id_sarana){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM inventaris WHERE id_sarana='$id_sarana'");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;

    }
    // END INVENTARIS

    function detail_lihat($id_sarana){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM inventaris i LEFT JOIN jenis j ON i.id_jenis=j.id_jenis LEFT JOIN ruang r ON i.id_ruang=r.id_ruang LEFT JOIN sarana s ON i.id_sarana=s.id_sarana LEFT JOIN petugas p ON i.id_petugas=p.id_petugas WHERE i.id_sarana='$id_sarana'");
		while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;

    }

    function tambah_inventaris($nama_inventaris,$kondisi,$jumlah,$keterangan,$jenis,$ruang,$sarana,$kode_inventaris,$id_petugas){
    	$tgl=date("Y-m-d");
    	$foto = $_FILES['foto']['name'];
	 	$lokasi = $_FILES['foto']['tmp_name'];
	 	$upload = move_uploaded_file($lokasi, "../assets/images/inventaris/".$foto);
    	$data = mysqli_query($this->koneksi,"INSERT INTO inventaris (nama,kondisi,keterangan,jumlah,id_jenis,tanggal_register,id_ruang,kode_inventaris,id_sarana,id_petugas,foto) VALUES ('$nama_inventaris','$kondisi','$keterangan','$jumlah','$jenis','$tgl','$ruang','$kode_inventaris','$sarana','$id_petugas','$foto')");
    }

     function hapus_inventaris($id_inventaris){
        mysqli_query($this->koneksi,"DELETE FROM inventaris WHERE id_inventaris='$id_inventaris'");
    }

     function edit_inventaris($id_inventaris){
		$data = mysqli_query($this->koneksi,"SELECT * FROM inventaris WHERE id_inventaris='$id_inventaris'");
		while($in = mysqli_fetch_array($data)){
			$hasil[] = $in;
		}
		return $hasil;
	}
 
	function update_inventaris($id_inventaris,$nama_inventaris,$kondisi,$jumlah,$keterangan,$jenis,$ruang,$sarana,$kode_inventaris,$id_petugas){
		$foto = $_FILES['foto']['name'];
		$lokasi = $_FILES['foto']['tmp_name'];
		if (!empty($lokasi)) {			
		 	$upload = move_uploaded_file($lokasi, "../assets/images/inventaris/".$foto);
			mysqli_query($this->koneksi,"UPDATE inventaris set nama='$nama_inventaris',kondisi='$kondisi',keterangan='$keterangan',jumlah='$jumlah',id_jenis='$jenis',id_ruang='$ruang',kode_inventaris='$kode_inventaris',id_sarana='$sarana',foto='$foto',id_petugas='$id_petugas' where id_inventaris='$id_inventaris'");
		}else{
			mysqli_query($this->koneksi,"UPDATE inventaris set nama='$nama_inventaris',kondisi='$kondisi',keterangan='$keterangan',jumlah='$jumlah',id_jenis='$jenis',id_ruang='$ruang',kode_inventaris='$kode_inventaris',id_sarana='$sarana',id_petugas='$id_petugas' where id_inventaris='$id_inventaris'");
		}
	 	
	}

    function pinjam_adm($id,$id_inventaris,$kode_pjm,$nama_inventaris,$jumlah){

    	$status = 'Pinjam';
    	$tanggal = date("Y-m-d");

    	$masuk1 = mysqli_query($this->koneksi,"INSERT INTO peminjaman(kode_peminjaman_p,tanggal_pinjam,status_peminjaman,id_inventaris,id_petugas) VALUES ('$kode_pjm','$tanggal','$status','$id_inventaris','$id')");

    	$ds=mysqli_fetch_array(mysqli_query($this->koneksi,"SELECT id_peminjaman FROM peminjaman ORDER BY id_peminjaman DESC LIMIT 1"));
        $id_peminjaman=$ds['id_peminjaman'];

    	$masuk2 = mysqli_query($this->koneksi,"INSERT INTO detail_pinjam(kode_peminjaman,id_inventaris,jumlah_p,id_peminjaman)VALUES('$kode_pjm','$id_inventaris','$jumlah','$id_peminjaman')");

    	$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah - $jumlah WHERE id_inventaris = '$id_inventaris'");

    }
    function tam_b_p_adm($kode_pjm,$id_inventaris,$jumlah){
    	$t_p= mysqli_query($this->koneksi,"UPDATE detail_pinjam SET jumlah_p=jumlah_p+$jumlah WHERE kode_peminjaman='$kode_pjm' AND id_inventaris='$id_inventaris'");
    	$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah - $jumlah WHERE id_inventaris = '$id_inventaris'");
    }

    function kurangi($kode_pjm,$id_inventaris,$jumlah){
    	$kur= mysqli_query($this->koneksi,"UPDATE detail_pinjam SET jumlah_p=jumlah_p-$jumlah WHERE kode_peminjaman='$kode_pjm' AND id_inventaris='$id_inventaris'");

		$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah + $jumlah WHERE id_inventaris = '$id_inventaris'");
    	
    }
    function hapus_p($kode_pjm,$id_detail_p,$id_peminjaman,$jumlah_p,$id_inventaris){
    	$hapus= mysqli_query($this->koneksi,"DELETE FROM peminjaman WHERE kode_peminjaman_p='$kode_pjm' AND id_peminjaman = '$id_peminjaman'");
    	$hapus2= mysqli_query($this->koneksi,"DELETE FROM detail_pinjam WHERE kode_peminjaman='$kode_pjm' AND id_detail_pinjam = '$id_detail_p'");
    	$hapus_up= mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah + $jumlah_p WHERE id_inventaris='$id_inventaris'");
    	
    }

    function detail_p_b($kode_pjm,$id_inventaris){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM detail_pinjam LEFT JOIN inventaris ON detail_pinjam.id_inventaris = inventaris.id_inventaris LEFT JOIN peminjaman ON detail_pinjam.kode_peminjaman=peminjaman.kode_peminjaman_p WHERE detail_pinjam.kode_peminjaman = '$kode_pjm' GROUP BY detail_pinjam.id_inventaris");
		while($pt = mysqli_fetch_array($data)){
			
			$hasil[] = $pt;
		}
		return $hasil;
    }

    // ---------------- Pegawai ----------------------------

    function pinjam_p($id,$id_inventaris,$kode_pjm,$nama_inventaris,$jumlah){

    	$status = 'Pinjam';
    	$tanggal = date("Y-m-d");

    	$masuk1 = mysqli_query($this->koneksi,"INSERT INTO peminjaman_p(kode_peminjaman_pe,tanggal_pinjam,status_peminjaman_pe,id_inventaris,id_pegawai) VALUES ('$kode_pjm','$tanggal','$status','$id_inventaris','$id')");

    	$ds=mysqli_fetch_array(mysqli_query($this->koneksi,"SELECT id_peminjaman_p FROM peminjaman_p ORDER BY id_peminjaman_p DESC LIMIT 1"));
        $id_peminjaman=$ds['id_peminjaman_p'];

    	$masuk2 = mysqli_query($this->koneksi,"INSERT INTO detail_pinjam_p(kode_peminjaman_d_p,id_inventaris,jumlah_p_p,id_peminjaman_p)VALUES('$kode_pjm','$id_inventaris','$jumlah','$id_peminjaman')");

    	$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah - $jumlah WHERE id_inventaris = '$id_inventaris'");

    }
    function tam_b_p_p($kode_pjm,$id_inventaris,$jumlah){
    	$t_p= mysqli_query($this->koneksi,"UPDATE detail_pinjam_p SET jumlah_p_p=jumlah_p_p+$jumlah WHERE kode_peminjaman_d_p='$kode_pjm' AND id_inventaris='$id_inventaris'");
    	$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah - $jumlah WHERE id_inventaris = '$id_inventaris'");
    }

    function kurangi_p($kode_pjm,$id_inventaris,$jumlah){
    	$kur= mysqli_query($this->koneksi,"UPDATE detail_pinjam_p SET jumlah_p_p=jumlah_p_p-$jumlah WHERE kode_peminjaman_d_p='$kode_pjm' AND id_inventaris='$id_inventaris'");

		$sql2 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah + $jumlah WHERE id_inventaris = '$id_inventaris'");
    	
    }
    function hapus_p_p($kode_pjm,$id_detail_p,$id_peminjaman,$jumlah_p,$id_inventaris){
    	$hapus= mysqli_query($this->koneksi,"DELETE FROM peminjaman_p WHERE kode_peminjaman_pe='$kode_pjm' AND id_peminjaman_p = '$id_peminjaman'");
    	$hapus2= mysqli_query($this->koneksi,"DELETE FROM detail_pinjam_p WHERE kode_peminjaman_d_p='$kode_pjm' AND id_detail_pinjam_p = '$id_detail_p'");
    	$hapus_up= mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah = jumlah + $jumlah_p WHERE id_inventaris='$id_inventaris'");
    	
    }

    function detail_p_b_p($kode_pjm,$id_inventaris){
    	$data = mysqli_query($this->koneksi,"SELECT * FROM detail_pinjam_p LEFT JOIN inventaris ON detail_pinjam_p.id_inventaris = inventaris.id_inventaris LEFT JOIN peminjaman_p ON detail_pinjam_p.kode_peminjaman_d_p=peminjaman_p.kode_peminjaman_pe WHERE detail_pinjam_p.kode_peminjaman_d_p = '$kode_pjm' GROUP BY detail_pinjam_p.id_inventaris");
		while($pt = mysqli_fetch_array($data)){
			
			$hasil[] = $pt;
		}
		return $hasil;
    }

    // // ------------- Pengembalian -----------------------

	function pengembalian_table1(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM detail_pinjam LEFT JOIN inventaris ON detail_pinjam.id_inventaris=inventaris.id_inventaris LEFT JOIN peminjaman ON detail_pinjam.kode_peminjaman=peminjaman.kode_peminjaman_p LEFT JOIN petugas ON peminjaman.id_petugas =petugas.id_petugas GROUP BY peminjaman.kode_peminjaman_p ORDER BY peminjaman.id_peminjaman DESC");
    	while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
		$no = 1;
	}
    // OP
	function pengembalian_table2(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM detail_pinjam LEFT JOIN inventaris ON detail_pinjam.id_inventaris=inventaris.id_inventaris LEFT JOIN peminjaman ON detail_pinjam.kode_peminjaman=peminjaman.kode_peminjaman_p LEFT JOIN petugas ON peminjaman.id_petugas =petugas.id_petugas WHERE petugas.id_level='2' GROUP BY peminjaman.kode_peminjaman_p ORDER BY peminjaman.id_peminjaman DESC");
    	while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}

	function pengembalian_table3(){
		$data = mysqli_query($this->koneksi,"SELECT * FROM detail_pinjam_p LEFT JOIN inventaris ON detail_pinjam_p.id_inventaris=inventaris.id_inventaris LEFT JOIN peminjaman_p ON detail_pinjam_p.kode_peminjaman_d_p=peminjaman_p.kode_peminjaman_pe LEFT JOIN pegawai ON peminjaman_p.id_pegawai=pegawai.id_pegawai GROUP BY peminjaman_p.kode_peminjaman_pe ORDER BY peminjaman_p.id_peminjaman_p DESC");
    	while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}

	function pengembalian_table1_detail($kode_pjm){
		$data = mysqli_query($this->koneksi,"SELECT * FROM peminjaman p LEFT JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN inventaris i ON p.id_inventaris=i.id_inventaris WHERE p.kode_peminjaman_p='$kode_pjm'");
    	while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}

	function pengembalian_table2_detail_p($kode_pjm){
		$data = mysqli_query($this->koneksi,"SELECT * FROM peminjaman_p p LEFT JOIN detail_pinjam_p d ON p.id_peminjaman_p=d.id_peminjaman_p LEFT JOIN inventaris i ON p.id_inventaris=i.id_inventaris WHERE p.kode_peminjaman_pe='$kode_pjm'");
    	while($pt = mysqli_fetch_array($data)){
			$hasil[] = $pt;
		}
		return $hasil;
	}

	function kembali_b($id_peminjaman,$jumlah,$id_inventaris){
		$tanggal_k=date("Y-m-d");
		$status = 'Kembali';
		$sql1 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah=jumlah+$jumlah WHERE id_inventaris = '$id_inventaris'");
		$sql2 = mysqli_query($this->koneksi,"UPDATE peminjaman SET status_peminjaman ='$status',tanggal_kembali='$tanggal_k' WHERE id_peminjaman='$id_peminjaman'");

		
	}
	function kembali_b_p($id_peminjaman_p,$jumlah,$id_inventaris){
		$tanggal_k=date("Y-m-d");
		$status = 'Kembali';
		$sql1 = mysqli_query($this->koneksi,"UPDATE inventaris SET jumlah=jumlah+$jumlah WHERE id_inventaris = '$id_inventaris'");
		$sql2 = mysqli_query($this->koneksi,"UPDATE peminjaman_p SET status_peminjaman_pe ='$status',tanggal_kembali='$tanggal_k' WHERE id_peminjaman_p='$id_peminjaman_p'");

		
	}
    // ------------- Akhir Pengembalian Petugas -----------------

}