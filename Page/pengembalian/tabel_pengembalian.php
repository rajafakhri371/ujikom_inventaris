<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Peminjaman Petugas</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                  
                                        <th scope="col">Tanggal Kembali</th>                                  
                                        <th scope="col">Status</th>                                  
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table1($_GET['id_inventaris']) as $tb){
                                    
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_petugas']; ?></td>
                                    <td><?php echo $tb['kode_peminjaman_p']; ?></td>
                                    <td><?php echo $tb['tanggal_pinjam']; ?></td>
                                    <td><?php echo $tb['tanggal_kembali']; ?></td>
                                    <td><?php echo $tb['status_peminjaman']; ?></td>
                                    <td>
                                        <a href="?page=pengembalian&opsi=tabel_view&kode_peminjaman=<?php echo $tb['kode_peminjaman']; ?>"><i class="ti-pencil-alt"></i></a>       
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
</div>