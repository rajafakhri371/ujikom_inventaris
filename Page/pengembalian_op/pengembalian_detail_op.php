<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Petugas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pegawai</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Siswa</a>
                                    </li>
                                </ul>
                    <div class="tab-content mt-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <p><h4 class="header-title">Data Peminjaman Petugas</h4>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                                                 
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table2() as $tb){                                   

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_petugas']; ?></td>
                                    <td><?php echo $tb['kode_peminjaman_p']; ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_p'];
                                         $sql=$konek->query("SELECT status_peminjaman FROM peminjaman WHERE kode_peminjaman_p='$k' AND status_peminjaman='Pinjam'");
                                         $cek = $sql->num_rows;
                                         $data = $sql->fetch_array();
                                         if ($cek >= 1) {
                                             echo"Pinjam";
                                         }else{
                                            echo "Kembali";
                                         }
                                          
                                    ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_p'];
                                         $sum=$konek->query("SELECT SUM(jumlah_p) as total FROM detail_pinjam WHERE kode_peminjaman='$k'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                    ?></td>
                                    <td><?php echo date('d F Y', strtotime($tb['tanggal_pinjam'])); ?></td>
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_view_op&kode_peminjaman=<?php echo $tb['kode_peminjaman']; ?>&nama_petugas=<?php echo $tb['nama_petugas']; ?>"><i class="btn btn-primary ti-eye"></i></a>       
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <p><h4 class="header-title">Data Peminjaman Petugas</h4>
                        <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example2">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                                                 
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?php
                                $no = 1;
                                foreach($db->pengembalian_table3($kode_pjm) as $tb){

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_pegawai']; ?></td>
                                    <td><?php echo $tb['kode_peminjaman_d_p']; ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_d_p'];
                                         $sum=$konek->query("SELECT SUM(jumlah_p_p) as total FROM detail_pinjam_p WHERE kode_peminjaman_d_p='$k'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                    ?></td>
                                    <td><?php echo $tb['tanggal_pinjam']; ?></td>
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_view_op2&kode_peminjaman=<?php echo $tb['kode_peminjaman_d_p']; ?>&nama_pegawai=<?php echo $tb['nama_pegawai']; ?>&id_inventaris=<?php echo $tb['id_inventaris']; ?>"><i class="btn btn-primary ti-eye"></i></a>       
                                    </td>
                                </tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <p><h4 class="header-title">Data Peminjaman Siswa</h4>
                        <div class="single-table">
                        <div class="table-responsive">
                            <table class="table text-center" id="example3">
                                <thead class="text-uppercase bg-primary">
                                    <tr class="text-white">
                                        <th scope="col">NO</th>
                                        <th scope="col">Nama Peminjam</th>
                                        <th scope="col">Kode Peminjaman</th>
                                        <th scope="col">Jumlah Yang Di Pinjam</th>
                                        <th scope="col">Tanggal Pinjam</th>                                                                 
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
<!--                             <?php
                                $no = 1;
                                foreach($db->pengembalian_table1($kode_pjm) as $tb){
                                    $total +=$tb['jumlah_p']

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $no++; ?></th>
                                    <td><?php echo $tb['nama_petugas']; ?></td>
                                    <td><?php echo $tb['kode_peminjaman_p']; ?></td>
                                    <td><?php 
                                    $k=$tb['kode_peminjaman_p'];
                                         $sum=$konek->query("SELECT SUM(jumlah_p) as total FROM detail_pinjam WHERE kode_peminjaman='$k'");
                                          while($da=$sum->fetch_array()){
                                           echo $da['total'];
                                          }
                                    ?></td>
                                    <td><?php echo $tb['tanggal_pinjam']; ?></td>
                                    <td>
                                        <a href="?page=pengembalian_operator&opsi=tabel_view_op&kode_peminjaman=<?php echo $tb['kode_peminjaman_p']; ?>&nama_petugas=<?php echo $tb['nama_petugas']; ?>&id_inventaris=<?php echo $tb['id_inventaris']; ?>"><i class="btn btn-primary ti-eye"></i></a>       
                                    </td>
                                </tr>
                            <?php } ?> -->
                                </tbody>
                            </table>
                        </div>
                    </div></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>