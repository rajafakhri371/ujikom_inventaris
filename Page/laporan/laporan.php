<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                	<a href="page/laporan/laporan_inventaris.php" target="blank"><button type="button" class="btn btn-primary mb-3">Inventaris</button></a><br>

                    <a href="page/laporan/laporan_data_jenis.php" target="blank"><button type="button" class="btn btn-primary mb-3">Data Jenis</button></a><br>

                    <a href="page/laporan/laporan_data_ruang.php" target="blank"><button type="button" class="btn btn-primary mb-3">Data Ruang</button></a><br>

                    <a href="page/laporan/laporan_data_petugas.php" target="blank"><button type="button" class="btn btn-primary mb-3">Data Petugas</button></a><br>

                    <a href="page/laporan/laporan_data_pegawai.php" target="blank"><button type="button" class="btn btn-primary mb-3">Data Pegawai</button></a><br>

                    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target=".bd-example-modal-sm">Peminjaman</button>
                    <div class="col-lg-6 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="modal fade bd-example-modal-sm">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Peminjaman</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="page/laporan/laporan_peminjaman.php" method="POST">
                                                    <label for="example-text-input" class="col-form-label">Lihat Data Dari Tanggal</label>
                                                    <input type="date" class="form-control" name="tanggal_awal">
                                                    <label for="example-text-input" class="col-form-label">Sampai Tanggal</label>
                                                    <input type="date" class="form-control" name="tanggal_akhir">
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>