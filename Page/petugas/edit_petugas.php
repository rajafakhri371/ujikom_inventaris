<?php
foreach($db->edit_petugas($_GET['id_petugas']) as $d){
?>

<div class="col-lg-6 col-ml-12">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Data Ruangan</h3>
                    <p class="text-muted font-14 mb-4">Pastikan Untuk Mengisi Data Dengan Benar</p>
                    <form action="function/proses.php?aksi=update_petugas" method="POST">
                    <div class="form-group">
                        <input class="form-control" type="hidden" name="id_petugas" id="example-text-input" required="" value="<?php echo $d['id_petugas']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Nama Petugas</label>
                        <input class="form-control" type="text" name="nama_petugas" maxlength="25" id="example-text-input" required="" value="<?php echo $d['nama_petugas'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Username</label>
                        <input class="form-control" type="text" name="username" maxlength="25" id="example-text-input" required="" value="<?php echo $d['username'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Password</label>
                        <input class="form-control" type="Password" name="password" maxlength="25" id="example-text-input" required="" value="<?php echo $d['password'] ?>">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Aktivasi</label>
                        <select class="form-control" name="level" style="height: calc(3rem + 2px);">
                            <?php 
                                foreach($db->tampil_data_level() as $js){
                                    if ($js['id_level']==$d['id_level']) {
                                        $selected="selected";
                                    }else{
                                        $selected="";
                                    }
                            ?>
                            <option value="<?php echo $js['id_level'] ?>" <?php echo $selected ?> ><?php echo $js['nama_level'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4"> Simpan</button>
                    <?php } ?>
                    </form>
                </div>
            </div>
        </div>
</div>