
<div class="row">
<div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                	<div style="width: 800px;margin: 0px auto;">
        <canvas id="myChart"></canvas>
    </div>
            <script>
            <?php 
            $sql = $konek->query("SELECT * FROM inventaris");
            $sql2 = $konek->query("SELECT * FROM inventaris");
             ?>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels:[<?php while ($nm = $sql->fetch_array()) { echo '"' . $nm['nama'] . '",';}?>],
                    datasets: [{
                            label:'Daftar List Jumlah Barang',
                            data: [<?php while ($jml = $sql2->fetch_array()) { echo '"' . $jml[jumlah] . '",';}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
</script>
                </div>
            </div>
        </div>
    </div>