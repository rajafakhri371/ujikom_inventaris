<?php 
    session_start();
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

    include"kode_pinjam.php";

    $konek = new mysqli("localhost","root","","uji_kom");

    include 'function/function.php';
    $db = new Database();
    
    if ($_SESSION['Administrator'] || $_SESSION['Petugas'] || $_SESSION['Pegawai']) {

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Inventaris Sarana Dan Prasarana SMK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" type="text/css" href="assets/data_table/assets/css/jquery.dataTables.css">
    
    <!-- amchart css -->
<!--     <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" /> -->
    <!-- others css -->
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="assets/Chart.js/Chart.js"></script>
    <script src="assets/Chart.js/Chart.bundle.min.js"></script>

</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.html"><img src="assets/images/icon/logo.png" alt="logo"></a>
                </div>
            </div>
 

            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li class="active">
                                <a href="index.php" aria-expanded="true"><i class="ti-dashboard"></i><span>dashboard</span></a>
                            </li>
                            <?php 
                                if ($_SESSION['Administrator']){

                            ?> 
                            <li>
                                <a href="?page=inventaris" aria-expanded="true"><i class="fa fa-briefcase"></i><span>Inventaris</span></a>
                            </li>
                            <li>
                                <a href="?page=ruang" aria-expanded="true"><i class="fa fa-building"></i><span>Ruangan</span></a>
                            </li>
                            <li>
                                <a href="?page=jenis" aria-expanded="true"><i class="ti-view-list-alt"></i><span>Jenis</span></a>
                            </li>
                            <li>
                                <a href="?page=petugas" aria-expanded="true"><i class="ti-user"></i><span>Petugas</span></a>
                            </li>
                            <li>
                                <a href="?page=pegawai" aria-expanded="true"><i class="fa fa-users"></i><span>Pegawai</span></a>
                            </li>
                            <li>
                                <a href="?page=peminjaman" aria-expanded="true"><i class="ti-layout-media-right"></i><span>Peminjam</span></a>
                            </li>
                            <li>
                                <a href="#" aria-expanded="true"><i class="ti-layout-media-right"></i><span>Peminjam Siswa</span></a>
                            </li>

                            <li>
                                <a href="?page=pengembalian" aria-expanded="true"><i class="ti-layout-list-large-image"></i><span>Pengambalian</span></a>
                            </li>
                            <li>
                                <a href="?page=laporan" aria-expanded="true"><i class="fa fa-file-pdf-o"></i><span>Laporan</span></a>
                            </li>
                            <?php 
                                }
                            ?>
                            <?php
                                if ($_SESSION['Petugas']){
                                
                            ?> 
                            <li>
                                <a href="?page=peminjaman_operator" aria-expanded="true"><i class="ti-layout-media-right"></i><span>Peminjam Operator</span></a>
                            </li>
                            <li>
                                <a href="#" aria-expanded="true"><i class="ti-layout-media-right"></i><span>Peminjam Siswa</span></a>
                            </li>
                            <li>
                                <a href="?page=pengembalian_operator" aria-expanded="true"><i class="ti-layout-list-large-image"></i><span>Pengambalian</span></a>
                            </li>
                            <?php
                             }
                             ?>
                             <?php
                               if ($_SESSION['Pegawai']) {
                            ?>
                            <li>
                                <a href="?page=peminjaman_pegawai" aria-expanded="true"><i class="ti-layout-media-right"></i><span>Peminjam Pegawai</span></a>
                            </li>
                            <?php
                               }

                             ?>
                            
                            
                            <li>
                                <a href="#" aria-expanded="true"><i class="ti-layout-cta-btn-right"></i><span>Request</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                   <?php 
                   if ($_SESSION['Administrator'] || $_SESSION['Petugas']) {
                       
                   
                   ?>
                <div class="col-md-6 col-sm-8 clearfix">
                <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-12 clearfix">
                        <div class="user-profile pull-right">
                            <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['nama_petugas'] ?><i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Profile</a>
                                <a class="dropdown-item" href="logout.php">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php }elseif($_SESSION['Pegawai']) { ?>
                <div class="col-md-6 col-sm-8 clearfix">
                <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-12 clearfix">
                        <div class="user-profile pull-right">
                            <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['nama_pegawai'] ?><i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Profile</a>
                                <a class="dropdown-item" href="logout.php">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>

                </div>
            </div>

<div class="main-content-inner">
<?php
                $page = $_GET['page'];
                $opsi = $_GET['opsi'];

                if ($page == "inventaris") {
                    if ($opsi == "") {
                        include "page/inventaris/inventaris.php";
                    }

                    if ($opsi == "detail") {
                        include"page/inventaris/detail.php";
                    }

                    if ($opsi == "tambah") {
                        include"page/inventaris/tambah_inventaris.php";
                    }

                    if ($opsi == "edit_inventaris") {
                        include"page/inventaris/edit_inventaris.php";
                    }

                }

                if ($page == "ruang") {
                    if ($opsi == "") {
                        include "page/ruang/tampil_ruang.php";
                    }

                    if ($opsi == "tambah") {
                        include"page/ruang/tambah_ruang.php";
                    }

                    if ($opsi == "edit") {
                        include"page/ruang/edit_ruang.php";
                    }

                }

                if ($page == "jenis") {
                    if ($opsi == "") {
                        include "page/jenis/tampil_jenis.php";
                    }

                    if ($opsi == "tambah") {
                        include"page/jenis/tambah_jenis.php";
                    }

                    if ($opsi == "edit") {
                        include"page/jenis/edit_jenis.php";
                    }
                }

                if ($page == "petugas") {
                    if ($opsi == "") {
                        include "page/petugas/tampil_petugas.php";
                    }

                    if ($opsi == "tambah") {
                        include"page/petugas/tambah_petugas.php";
                    }

                    if ($opsi == "edit") {
                        include"page/petugas/edit_petugas.php";
                    }
                }

                if ($page == "pegawai") {
                    if ($opsi == "") {
                        include "page/pegawai/tampil_pegawai.php";
                    }

                    if ($opsi == "tambah") {
                        include"page/pegawai/tambah_pegawai.php";
                    }

                    if ($opsi == "edit") {
                        include"page/pegawai/edit_pegawai.php";
                    }
                }

                if ($page == "peminjaman") {
                    if ($opsi == "") {
                        include "page/peminjaman/peminjaman.php";
                    }

                    if ($opsi == "peminjaman_detail") {
                        include"page/peminjaman/peminjaman_detail.php";
                    }

                }

                if ($page == "peminjaman_operator") {
                    if ($opsi == "") {
                        include "page/peminjaman_op/peminjaman_op.php";
                    }

                    if ($opsi == "peminjaman_detail_op") {
                        include"page/peminjaman_op/peminjaman_detail_op.php";
                    }

                }

                if ($page == "peminjaman_pegawai") {
                    if ($opsi == "") {
                        include "page/peminjaman_p/peminjaman_p.php";
                    }

                    if ($opsi == "peminjaman_detail_op") {
                        include"page/peminjaman_p/peminjaman_detail_p.php";
                    }

                }

                if ($page == "peminjaman_siswa") {
                    if ($opsi == "") {
                        include "page/peminjaman_siswa/peminjaman_s.php";
                    }

                    if ($opsi == "peminjaman_s_detail") {
                        include"page/peminjaman_siswa/peminjaman_s_detail.php";
                    }

                }

                if ($page == "pengembalian") {
                    if ($opsi == "") {
                        include"page/pengembalian/pengembalian.php";
                    }
                    if ($opsi =="detail_pengembalian") {
                        include"page/pengembalian/pengembalian_detail.php";
                    }
                    if ($opsi == "tabel_pengembalian"){
                        include"page/pengembalian/tabel_pengembalian.php";
                    }
                    if ($opsi == "tabel_view") {
                        include"page/pengembalian/tabel_pengembalian_view.php";
                    }
                    if ($opsi == "tabel_view_2") {
                        include"page/pengembalian/tabel_pengembalian_view_2.php";
                    }
                }

                if ($page == "pengembalian_operator") {
                    if ($opsi == "") {
                        include"page/pengembalian_op/pengembalian_op.php";
                    }
                    if ($opsi =="detail_pengembalian_op") {
                        include"page/pengembalian_op/pengembalian_detail_op.php";
                    }
                    if ($opsi == "tabel_pengembalian_op"){
                        include"page/pengembalian_op/tabel_pengembalian_op.php";
                    }
                    if ($opsi == "tabel_view_op") {
                        include"page/pengembalian_op/tabel_pengembalian_view_op.php";
                    }
                    if ($opsi == "tabel_view_op2") {
                        include"page/pengembalian_op/tabel_pengembalian_view_op2.php";
                    }
                }

                if ($page == "laporan") {
                    if ($opsi == "") {
                        include"page/laporan/laporan.php";
                    }
                }

                if ($page == "") {
                   include "home.php";
                }
                // if ($page == "peminjaman_petugas") {
                //     if ($opsi == "") {
                //         include"page/peminjaman/peminjaman_p.php";
                //     }
                //     if ($opsi == "peminjaman_detail_p") {
                //         include"page/peminjaman/peminjaman_detail_p.php";
                //     }
                // }
                // if ($page == "pengembalian_petugas") {
                //     if ($opsi == "") {
                //         include"page/pengembalian/pengembalian_p.php";
                //     }
                //     if ($opsi =="detail_pengembalian_p") {
                //         include"page/pengembalian/pengembalian_detail_p.php";
                //     }
                //     if ($opsi == "tabel_pengembalian_p"){
                //         include"page/pengembalian/tabel_pengembalian_p.php";
                //     }
                // }
            ?>

</div>
        </div>
        <footer>
            <div class="footer-area">
                <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
            </div>
        </footer>
        <!-- footer area end-->
    </div>
    <!-- page container area end -->
    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/scripts.js"></script>
    <script type="text/javascript" src="assets/data_table/assets/js/jquery.dataTables.min.js"></script>
    
<script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#example2').DataTable();
        } );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#example3').DataTable();
        } );
    </script>
</body>

</html>
<?php  
    }
    else{
        echo "<script type='text/javascript'>window.location='login.php'</script>";
    }
 ?>            
